%{!?netsnmp_check: %global netsnmp_check 1}
%global multilib_arches  x86_64  aarch64

Name:            net-snmp
Version:         5.9.3
Release:         6
Epoch:           1
Summary:         SNMP Daemon
License:         BSD
URL:             http://net-snmp.sourceforge.net/
Source0:         https://sourceforge.net/projects/net-snmp/files/net-snmp/%{version}/net-snmp-%{version}.tar.gz
Source1:         net-snmp.conf
Source2:         net-snmp-config.h
Source3:         net-snmp-config
Source4:         net-snmp-trapd.conf
Source5:         net-snmpd.sysconfig
Source6:         net-snmptrapd.sysconfig
Source7:         net-snmp-tmpfs.conf
Source8:         snmpd.service
Source9:         snmptrapd.service
Source10:        IETF-MIB-LICENSE.txt
Patch1:          backport-net-snmp-5.9-pie.patch
Patch2:          backport-net-snmp-5.9-dir-fix.patch
Patch3:          backport-net-snmp-5.9-multilib.patch
Patch4:          backport-net-snmp-5.9-test-debug.patch
Patch5:          backport-net-snmp-5.7.2-cert-path.patch
Patch6:          backport-net-snmp-5.9-cflags.patch
Patch7:          backport-net-snmp-5.8-Remove-U64-typedef.patch
Patch8:          backport-net-snmp-5.7.3-iterator-fix.patch
Patch9:          backport-net-snmp-5.9-autofs-skip.patch
Patch10:         backport-net-snmp-5.9-coverity.patch
Patch11:         backport-net-snmp-5.8-expand-SNMPCONFPATH.patch
Patch12:         backport-net-snmp-5.8-duplicate-ipAddress.patch
Patch13:         backport-net-snmp-5.9-memory-reporting.patch
Patch14:         backport-net-snmp-5.8-man-page.patch
Patch15:         backport-net-snmp-5.8-ipAddress-faster-load.patch
Patch16:         backport-net-snmp-5.8-rpm-memory-leak.patch
Patch17:         backport-net-snmp-5.9-aes-config.patch
Patch18:         backport-net-snmp-5.8-modern-rpm-api.patch
Patch19:         backport-net-snmp-5.9-python3.patch
Patch20:         backport-net-snmp-5.9.1-autoconf.patch
Patch21:         backport-CVE-2022-44792_CVE-2022-44793.patch
Patch22:         backport-libsnmp-Remove-netsnmp_openssl_err_log.patch
Patch23:         backport-net-snmp-5.9-ipv6-disable-leak.patch
Patch24:         backport-net-snmp-5.9-sendmsg-error-code.patch

patch25:         dump-space-around-the-equal-for-shellcheck-sc1068.patch
Patch26:         net-snmp-5.9.1-IdeaUI_antic_attack.patch
Patch27:         net-snmp-5.9.1-IdeaUI_reset_last_engineTime.patch
Patch28:         backport-Add-Linux-6.7-compatibility-parsing-proc-net-snmp.patch
Patch29:         backport-snmplib-Handle-two-oldEngineID-lines-in-snmpd.conf.-.patch
Patch30:         backport-libsnmp-Fix-a-buffer-overflow-in-setup_engineID.patch
Patch31:         backport-snmplib-Fix-memory-leaks-in-the-parse_enumlist-error.patch

%{?systemd_requires}
BuildRequires:   systemd gcc openssl-devel bzip2-devel elfutils-devel libselinux-devel
BuildRequires:   elfutils-libelf-devel rpm-devel perl-devel perl(ExtUtils::Embed) procps
BuildRequires:   python3-devel python3-setuptools chrpath mariadb-connector-c-devel net-tools
BuildRequires:   perl(TAP::Harness) lm_sensors-devel autoconf automake
Requires:        perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
Requires:        %{name}-libs = %{epoch}:%{version}-%{release}

Provides:        %{name}-utils
Obsoletes:       %{name}-utils
Provides:        %{name}-agent-libs
Obsoletes:       %{name}-agent-libs

%description
Net-SNMP is a suite of applications used to implement SNMP v1, SNMP v2c
and SNMP v3 using both IPv4 and IPv6. The suite includes:

- An extensible agent for responding to SNMP queries including built-in
  support for a wide range of MIB information modules
- Command-line applications to retrieve and manipulate information from
  SNMP-capable devices
- A daemon application for receiving SNMP notifications
- A library for developing new SNMP applications, with C and Perl APIs
- A graphical MIB browser.

%package         libs
Summary:         Libraries for %{name}

%description     libs
Libraries for %{name}.

%package         devel
Summary:         Development files and Header files for %{name}
Requires:        %{name} = %{epoch}:%{version}-%{release}
Requires:        %{name}-libs = %{epoch}:%{version}-%{release}
Requires:        elfutils-devel rpm-devel elfutils-libelf-devel openssl-devel
Requires:        lm_sensors-devel perl-devel

%description     devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package         perl
Summary:         Perl5 SNMP Extension Module
Requires:        %{name} = %{epoch}:%{version}-%{release} perl-interpreter
Requires:        %{name}-libs = %{epoch}:%{version}-%{release}
BuildRequires:   perl-interpreter perl-generators

%description     perl
The Perl5 'SNMP' Extension Module v3.1.0 for the UCD SNMPv3 library.

%package         gui
Summary:         An interactive graphical MIB browser for SNMP
Requires:        perl-Tk net-snmp-perl = %{epoch}:%{version}-%{release}

%description     gui
The %{name}-gui package contains tkmib utility. It is also 
capable of sending or retrieving the SNMP management information 
to/from the remote agents interactively.

%package -n      python3-net-snmp
%{?python_provide:%python_provide python3-net-snmp}
Provides:        %{name}-python = %{version}-%{release}
Provides:        %{name}-python%{?_isa} = %{version}-%{release}
Obsoletes:       %{name}-python < %{version}-%{release}
Summary:         The Python 'netsnmp' module for the Net-SNMP
Requires:        %{name}-libs = %{epoch}:%{version}-%{release}

%description -n python3-net-snmp
The 'netsnmp' module provides a full featured, tri-lingual SNMP (SNMPv3, 
SNMPv2c, SNMPv1) client API. The 'netsnmp' module internals rely on the
Net-SNMP toolkit library.
%package_help

%prep
%autosetup -p1
cp %{SOURCE10} .

%build
autoreconf
MIBS="host agentx smux \
     ucd-snmp/diskio tcp-mib udp-mib mibII/mta_sendmail \
     ip-mib/ipv4InterfaceTable ip-mib/ipv6InterfaceTable \
     ip-mib/ipAddressPrefixTable/ipAddressPrefixTable \
     ip-mib/ipDefaultRouterTable/ipDefaultRouterTable \
     ip-mib/ipv6ScopeZoneIndexTable ip-mib/ipIfStatsTable \
     sctp-mib rmon-mib etherlike-mib"
MIBS="$MIBS ucd-snmp/lmsensorsMib"
%configure --with-pcre=no  --enable-shared --enable-as-needed  --enable-embedded-perl \
    --enable-ipv6  --enable-local-smux --enable-mfd-rewrites \
    --enable-ucd-snmp-compatibility --sysconfdir=%{_sysconfdir} \
    --with-cflags="$RPM_OPT_FLAGS -D_RPM_4_4_COMPAT" \
    --with-ldflags="-Wl,-z,relro -Wl,-z,now" \
    --with-logfile="/var/log/snmpd.log" \
    --with-mib-modules="$MIBS" --with-mysql \
    --with-openssl --with-persistent-directory="/var/lib/net-snmp" \
    --with-perl-modules="INSTALLDIRS=vendor" --with-pic \
    --with-security-modules=tsm  --with-sys-location="Unknown" \
    --with-systemd  --with-temp-file-pattern=/var/run/net-snmp/snmp-tmp-XXXXXX \
    --with-transports="DTLSUDP TLSTCP" --with-sys-contact="root@localhost" <<EOF
EOF
cp libtool libtool.orig

%disable_rpath

make

find perl/blib -type f -name "*.so" -print -exec chrpath --delete {} \;

pushd python
%{__python3} setup.py --basedir="../" build
popd

%install
make install DESTDIR=%{buildroot}

basearch=%{_arch}

%ifarch %{multilib_arches}
mv %{buildroot}/%{_bindir}/net-snmp-config %{buildroot}/%{_bindir}/net-snmp-config-${basearch}
install -m 755 %SOURCE3 %{buildroot}/%{_bindir}/net-snmp-config
mv %{buildroot}/%{_includedir}/net-snmp/net-snmp-config.h %{buildroot}/%{_includedir}/net-snmp/net-snmp-config-${basearch}.h
install -m644 %SOURCE2 %{buildroot}/%{_includedir}/net-snmp/net-snmp-config.h
%endif

mkdir -p %{buildroot}%{_sysconfdir}/snmp
install -m 644 %SOURCE1 %{buildroot}%{_sysconfdir}/snmp/snmpd.conf
install -m 644 %SOURCE4 %{buildroot}%{_sysconfdir}/snmp/snmptrapd.conf

mkdir -p  %{buildroot}%{_sysconfdir}/sysconfig
install -m 644 %SOURCE5 %{buildroot}%{_sysconfdir}/sysconfig/snmpd
install -m 644 %SOURCE6 %{buildroot}%{_sysconfdir}/sysconfig/snmptrapd

mkdir -p  %{buildroot}%{_localstatedir}/lib/net-snmp/mib_indexes
mkdir -p  %{buildroot}%{_localstatedir}/lib/net-snmp/cert_indexes
mkdir -p  %{buildroot}%{_localstatedir}/run/net-snmp

rm -f %{buildroot}%{_bindir}/snmpinform
ln -s snmptrap %{buildroot}%{_bindir}/snmpinform
%delete_la_and_a

find %{buildroot} -name perllocal.pod  -o -name .packlist  -o -name "*.bs" \
                  -o -name Makefile.subs.pl | xargs -ri rm -f {}

install -m 644 local/mib2c.*.conf %{buildroot}%{_datadir}/snmp

pushd python
%{__python3} setup.py --basedir=.. install -O1 --skip-build --root %{buildroot} 
popd

find %{buildroot} -name '*.so' | xargs chmod 0755
dd bs=1024 count=250 if=ChangeLog of=ChangeLog.trimmed

for file in README COPYING; do
    iconv -f 8859_1 -t UTF-8 <$file >$file.utf8
    mv $file.utf8 $file
done
chmod 644 local/passtest local/ipf-mod.pl

mkdir -p %{buildroot}/%{_tmpfilesdir}
install -m 644 %SOURCE7 %{buildroot}/%{_tmpfilesdir}/net-snmp.conf
mkdir -p %{buildroot}/%{_unitdir}
install -m 644 %SOURCE8 %SOURCE9 %{buildroot}/%{_unitdir}/

%check
%if %{netsnmp_check}
cp -f libtool.orig libtool
rm -vf testing/fulltests/default/T200snmpv2cwalkall_simple
chmod 755 local/passtest
LD_LIBRARY_PATH=%{buildroot}/%{_libdir} make test
chmod 644 local/passtest
%endif

%post
%systemd_post snmpd.service snmptrapd.service

%preun
%systemd_preun snmpd.service snmptrapd.service

%postun
%systemd_postun_with_restart snmpd.service snmptrapd.service

%ldconfig_scriptlets 
%ldconfig_scriptlets libs

%files
%license COPYING 
%config(noreplace) %{_sysconfdir}/sysconfig/snmpd
%config(noreplace) %{_sysconfdir}/sysconfig/snmptrapd
%config(noreplace) %attr(0600,root,root) %{_sysconfdir}/snmp/snmpd.conf
%config(noreplace) %attr(0600,root,root) %{_sysconfdir}/snmp/snmptrapd.conf
%{_bindir}/agentxtrap
%{_prefix}/lib/systemd/system/*.service
%{_prefix}/lib/tmpfiles.d/net-snmp.conf
%{_sbindir}/snmp*
%dir %{_datadir}/snmp
%{_datadir}/snmp/snmpconf-data/*
%{_bindir}/net-snmp-create-v3-user
%{_bindir}/snmpconf
%{_bindir}/snmp[^c-]*
%{_bindir}/encode_keychange
%dir /var/run/net-snmp
%{_libdir}/libnetsnmpagent.so.*
%{_libdir}/libnetsnmphelpers.so.*
%{_libdir}/libnetsnmpmibs.so.*
%{_libdir}/libnetsnmptrapd.so.*
%exclude  %{_prefix}/lib/debug/usr/sbin/snmptrapd-5.8-1.oe1.aarch64.debug
%exclude  %{_bindir}/ipf-mod.pl
%exclude  %{_bindir}/fixproc
%exclude  %{_bindir}/snmpcheck
%exclude  %{_libdir}/libsnmp*

%files libs
%{_libdir}/libnetsnmp.so.*
%dir %{_datadir}/snmp
%{_datadir}/snmp/mibs/*
%dir %{_localstatedir}/lib/net-snmp
%dir %{_localstatedir}/lib/net-snmp/mib_indexes
%dir %{_localstatedir}/lib/net-snmp/cert_indexes

%files devel
%attr(0644,root,root)
%attr(0755,root,root) %{_bindir}/net-snmp-config*
%{_includedir}/*
%{_libdir}/libnet*.so
%{_libdir}/pkgconfig/*

%files perl
%{_bindir}/mib2c*
%{_bindir}/net-snmp-cert
%{_bindir}/checkbandwidth
%{_bindir}/snmp-bridge-mib
%{_bindir}/traptoemail
%{_libdir}/perl5/vendor_perl/auto/*SNMP/*
%dir %{_libdir}/perl5/vendor_perl/auto/Bundle/NetSNMP
%{_libdir}/perl5/vendor_perl/Bundle/MakefileSubs.pm
%{_libdir}/perl5/vendor_perl/NetSNMP/*.pm
%{_libdir}/perl5/vendor_perl/NetSNMP/agent/*.pm
%{_libdir}/perl5/vendor_perl/SNMP.pm
%{_datadir}/snmp/mib2c-data/*
%{_datadir}/snmp/mib2c*.conf
%{_datadir}/snmp/snmp_perl*

%files gui
%{_bindir}/tkmib
%attr(0644,root,root) %{_mandir}/man1/tkmib.1.gz

%files -n python3-net-snmp
%doc README
%{python3_sitearch}/*

%files help
%doc ChangeLog.trimmed EXAMPLE.conf FAQ NEWS TODO README.snmpv3
%doc README README.agent-mibs README.agentx README.krb5 
%doc local/passtest local/ipf-mod.pl IETF-MIB-LICENSE.txt
%doc README.thread AGENT.txt PORTING local/README.mib2c
%doc README.aix README.hpux11 README.osX README.Panasonic_AM3X.txt README.solaris README.win32
%attr(0644,root,root)
%{_mandir}/man1/agentxtrap.1.gz
%{_mandir}/man1/net-snmp-create-v3-user.1.gz
%{_mandir}/man1/snmpconf.1.gz
%{_mandir}/man5/snmp*.5.gz
%{_mandir}/man5/variables.5.gz
%{_mandir}/man8/snmp*.8.gz
%{_mandir}/man1/snmp*.1.gz
%{_mandir}/man1/encode_keychange.1.gz
%{_mandir}/man5/variables.5.gz
%{_mandir}/man5/snmp.conf.5.gz
%{_mandir}/man1/net-snmp-config.1.gz
%{_mandir}/man3/*.3.gz
%{_mandir}/man1/mib2c*.1.gz
%{_mandir}/man1/snmp-bridge-mib.1.gz
%{_mandir}/man1/traptoemail.1.gz
%{_mandir}/man3/NetSNMP*.3pm.gz
%{_mandir}/man5/mib2c.conf.5.gz
%{_mandir}/man1/fixproc*

%changelog
* Thu Dec 05 2024 liyunqing <liyunqing@kylinos.cn> - 1:5.9.3-6
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:Sync upstream patches to fix memory leaks

* Tue Oct 29 2024 xingwei <xingwei14@h-partners.com> - 1:5.9.3-5
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:Sync upstream patches to fix Buffer overflow

* Wed Sep 25 2024 xingwei <xingwei14@h-partners.com> - 1:5.9.3-4
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:Migration patch from upstream, add compatibility to parse 
       Linux 6.7 Ip header while keep support for previous versions.

* Mon Jul 22 2024 xingwei <xingwei14@h-partners.com> - 1:5.9.3-3
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix build with autoconf 2.72

* Fri May 10 2024 gaihuiying <eaglegai@163.com> - 1:5.9.3-2
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:Customized the anti-attacki/reset-last_engineTime function for IdeaUI
       change the permissions of passtest

* Mon Aug 07 2023 xingwei <xingwei14@h-partners.com> - 1:5.9.3-1
- Type:requirement
- CVE:NA
- SUG:NA
- DESC:update net-snmp to 5.9.3

* Wed May 24 2023 xingwei <xingwei14@h-partners.com> - 1:5.9.1-8
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:remove pcre dependency

* Fri Feb 03 2023 gaihuiying <eaglegai@163.com> - 1:5.9.1-7
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix build with openssl 3.0

* Fri Dec 30 2022 gaihuiying <eaglegai@163.com> - 1:5.9.1-6
- Type:CVE
- CVE:CVE-2022-44792 CVE-2022-44793
- SUG:NA
- DESC:fix CVE-2022-44792 CVE-2022-44793

* Fri Sep 30 2022 xingwei <xingwei14@h-partners.com> - 1:5.9.1-5
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC: IF-MIB, IP-FORWARD-MIB: Improve robustness
	IF-MIB: pass the network interface index to net
	IF-MIB: fix a memory leak
	IF-MIB: fix a recently introduced use after free
	IF-MIB: add a trailing newline to an error message

* Sat Aug 27 2022 gaihuiying <eaglegai@163.com> - 1:5.9.1-4
- Type:CVE
- CVE:CVE-2022-24805 CVE-2022-24806 CVE-2022-24807 CVE-2022-24808 CVE-2022-24809 CVE-2022-24810
- SUG:NA
- DESC:fix CVE-2022-24805 CVE-2022-24806 CVE-2022-24807
           CVE-2022-24808 CVE-2022-24809 CVE-2022-24810

* Mon Aug 08 2022 gaihuiying <eaglegai@163.com> - 5.9.1-3
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix build error which occasionally happened

* Thu Apr 14 2022 xinghe <xinghe2@h-partners.com> - 5.9.1-2
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:fix test failure caused by autoconf version

* Thu Dec 16 2021 gaihuiying <gaihuiying1@huawei.com> - 5.9.1-1
- Type:requirement
- CVE:NA
- SUG:NA
- DESC:update net-snmp to 5.9.1

* Fri Sep 17 2021 gaihuiying <gaihuiying1@huawei.com> - 5.8-15
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:Fix undefined behavior in asn_build_int()
       libsnmp: Rework parse_imports()
       libsnmp: Increase MAX_IMPORTS
       libsnmp: Allocate the module import list on the heap
       libsnmp: Fix a memory leak in a MIB parser error path
       libsnmp: Fix the getoid() error path

* Tue Dec 15 2020 xihaochen <xihaochen@huawei.com> - 5.8-14
- Type:requirement
- ID:NA
- SUG:NA
- DESC:remove sensitive words 

* Tue Oct 27 2020 majun <majun65@huawei.com> - 5.8-13
- Type:cves
- ID: CVE-2020-15861
- SUG:NA
- DESC: Fix  CVE-2020-15861

* Thu Oct 08 2020 wangxiaopeng <wangxiaopeng7@huawei.com> - 5.8-12
- Type:cves
- ID: CVE-2020-15862
- SUG:NA
- DESC: Fix  CVE-2020-15862

* Tue Sep 01 2020 yuboyun <yuboyun@huawei.com> - 5.8-11
- Type:NA
- ID:NA
- SUG:NA
- DESC: add yaml file

* Thu Jul 09 2020 zhouyihang <zhouyihang3@huawei.com> - 5.8-10
- Type:cves
- ID:CVE-2019-20892
- SUG:NA
- DESC: Fix CVE-2019-20892

* Fri Jun 19 2020 gaihuiying <gaihuiying1@huawei.com> - 5.8-9
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: adapt to  python new verion

* Thu Mar 12 2020 Buildteam <buildteam@openeuler.org> - 5.8-8
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:avoid triggering undefined shift left

* Tue Feb 18 2020 hexiujun <hexiujun1@huawei.com> - 5.8-7
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:unpack libs subpackage

* Fri Dec 20 2019 openEuler Buildteam <buildteam@openeuler.org> - 5.8-6
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:rename some conf files

* Sat Oct 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 5.8-5
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:change the directory of the license files

* Wed Sep 25 2019 openEuler Buildteam <buildteam@openeuler.org> - 5.8-4
- Type:bugfix
- ID:NA
- SUG:NA

* Tue Sep 24 2019 openEuler Buildteam <buildteam@openeuler.org> - 5.8-3
- Type:version fallback
- ID:NA
- SUG:NA

* Mon Sep 23 2019 openEuler Buildteam <buildteam@openeuler.org> - 5.8-2
- Package init

